const express = require('express');
const app = express();
const request = require('request');
const bodyParser = require('body-parser');
const path = require('path');
const fetch = require('node-fetch')
const FormData = require('form-data');

const htmlToText = require('html-to-text')

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({}));
app.use(express.static(path.join(__dirname, 'public')));

const token = (callback) => {
  const {WWS_OAUTH_URL, APP_ID, APP_SECRET} = process.env;
  var form = new FormData();
  form.append('grant_type', 'client_credentials');

  var auth = 'Basic ' + new Buffer(APP_ID + ':' + APP_SECRET).toString('base64');

  const requestOptions = {
    method: 'POST',
    headers: {
      'Authorization': auth
    },
    body: form
  };

  if (!APP_ID || !APP_SECRET) {
    throw new Error('Please provide the app id and app secret');
  }

  fetch(WWS_OAUTH_URL, requestOptions)
      .then(function (response) {
        if (!response.ok) {
          console.error('Error authenticating the app')
          callback(new Error(res.status));
        }

        return response.json();
      })
      .then(autoResult => {
        console.log ('token_type: ' + autoResult.token_type);
        console.log ('expires_in: ' + autoResult.expires_in);

        callback(null, autoResult.access_token);
      })
}

const spaces = (token, callback) => {
  const {WWS_API_URL} = process.env;
  const requestOptions = {
    method: 'POST',
    url: WWS_API_URL + '/graphql',
    headers: {
      jwt: token,
      'Content-Type': 'application/graphql'
    },
    body: `
      query {
        spaces (first: 50) {
          items {
            title
            id
          }
        }
      }`
  };

  request(requestOptions, function (err, response, body) {
    if (err || response.statusCode !== 200) {
      console.error('Error retrieving spaces')
      callback(err || new Error(res.statusCode));
    }

    const result = JSON.parse(body);
    console.log('Space query result ' + result.data.spaces.items.length);
    callback(null, result.data.spaces.items);
  });
};

const send = (spaceId, text, token, callback) => {
  const {WWS_API_URL} = process.env;
  const requestOptions = {
    method: 'POST',
    url: WWS_API_URL + '/v1/spaces/' + spaceId + '/messages',
    headers: {
      jwt: token,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      type: 'appMessage',
      version: 1.0,
      annotations: [{
        type: 'generic',
        version: 1.0,
        color: '#6CB7FB',
        title: 'Sample Message',
        text: text,
        actor: {
          name: 'yairov@harmon.ie',
          avatar: 'https://avatars0.githubusercontent.com/u/579625',
          url: 'https://github.com/yairov'
        }
      }]
    })
  };

  request(requestOptions, function (err, response, body) {
    if (err) {
      console.error('Error sending message')
      callback(err || new Error(res.statusCode));
    }

    const result = JSON.parse(body);
    console.log('Send result ' + result.created);
    callback(null, result);
  });
};

const focus = (text, token, callback) => {
  const {WWS_API_URL} = process.env;
  const targetUrl = WWS_API_URL + '/v1/focus'
  const options = {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${token}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      text: text
    })
  }

  fetch(targetUrl, options)
      .then(function (response) {
        if (!response.ok) {
          throw new Error(`${response.status} Error focusing the text`)
        }
        return response.json()
      })
      .then(authResult => {
        callback(null, authResult)
      })

  //request(requestOptions, function (err, response, body) {
  //  if (err) {
  //    console.error('Error sending message')
  //    callback(err || new Error(res.statusCode));
  //  }
  //
  //  const result = JSON.parse(body);
  //  console.log('Focus result ' + result);
  //  callback(null, result);
  //});
};

app.post('/api/focus', function (req, res, next) {
  token(function(error, accessToken) {
    if (error) {
      throw error;
    }

    const text = req.body.text;

    focus(text, accessToken, function(error, result) {
      res.send(result);
    });

    //spaces(accessToken, function(error, spaces) {
    //  if (error) {
    //    throw error;
    //  }
    //
    //  const name = 'collage-demo-space';
    //  const text = 'sample text';
    //  const space = spaces.filter(s => s.title === name)[0];
    //
    //  send(space.id, text, accessToken, function(error, result) {
    //    res.send(result);
    //  });
    //})
  })
});

app.post('/api/message', function (req, res, next) {
  token(function(error, accessToken) {
    if (error) {
      throw error;
    }

    const text = req.body.text;

    spaces(accessToken, function(error, spaces) {
      if (error) {
        throw error;
      }

      const name = 'collage worksapce';
      const space = spaces.filter(s => s.title === name)[0];

      send(space.id, text, accessToken, function(error, result) {
        res.send(result);
      });
    })
  })
});

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.code = 404;
  err.message = 'Not Found';
  next(err);
});

app.use(function(err, req, res, next) {
  var error = {
    code: err.code || 500,
    error: err.error || err.message
  };
  console.log('error:', error);
  next ? '' : '';
  res.status(error.code).json(error);
});

module.exports = app;