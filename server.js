require('dotenv').config({silent: true});

const server = require('./app');
const port = 3393;

server.listen(port, function() {
  console.log('Server running on port: %d', port);
});
